#!/bin/bash

#DM # ack
#DM ln -sf `pwd`/ack/ackrc ~/.ackrc

#DM # synergy
#DM ln -sf `pwd`/synergy/synergy.conf ~/.synergy.conf

# bins
mkdir -p ~/bin
ln -sf `pwd`/bin/list-terminal-colors ~/bin/list-terminal-colors

#DM # hyper
#DM ln -sf `pwd`/hyper/hyper.js ~/.hyper.js

# Plug (to be used with nvim)
# https://github.com/junegunn/vim-plug
curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# nvim
mkdir -p ~/.config/nvim
ln -sf `pwd`/nvim/init.vim ~/.config/nvim/

# tmux
ln -sf `pwd`/tmux/tmux.conf ~/.tmux.conf

#teamocil
ln -sf `pwd`/tmux/teamocil ~/.teamocil

#DM # oh-my-fish
#DM mkdir -p ~/.config/omf
#DM ln -sf `pwd`/omf/bundle ~/.config/omf/
#DM ln -sf `pwd`/omf/channel ~/.config/omf/
#DM ln -sf `pwd`/omf/fonts ~/.config/omf/
#DM ln -sf `pwd`/omf/init.fish ~/.config/omf/
#DM ln -sf `pwd`/omf/asdf.fish ~/.config/omf/
#DM ln -sf `pwd`/omf/theme ~/.config/omf/

echo "# Now for some manual stuff, sorry!"
echo "## nvim"
echo "To install the nvim plugins, open up vim and type ':PlugInstall'\n"
echo "## git"
echo "Setup your git config like so:\ngit config --global user.email "josh.rubyist@gmail.com"\ngit config --global user.name "Josh Adams""

