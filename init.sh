#!/bin/bash

sudo apt-get install -y git

# Got to Git repo
cd ~/neovimdotfiles

# Initilize the Reop.
git submodule init
git submodule update

# Got to home.
cd

# Install neovim
sudo apt-get install software-properties-common
sudo add-apt-repository ppa:neovim-ppa/stable
sudo apt-get update
sudo apt-get install neovim

# Need curl for neovim/Plug
sudo apt-get install curl

# Need ctags for neovim
sudo apt-get install exuberant-ctags

# Get ruby for Tmux Teamocil and possibly neovim plugins
sudo apt-get install ruby-dev

# Get python for neovim
sudo apt-get install python-dev python-pip python3-dev python3-pip
sudo pip3 install --upgrade neovim

# Get Tmux
sudo apt-get install tmux

# Get Teamocil
sudo gem install teamocil

# Setup.sh assumes it's running from it's own dir
cd ~/neovimdotfiles
~/neovimdotfiles/setup.sh





